//
//  CharacterDetailView.swift
//  Rick and Morty
//
//  Created by Ramzy Kermad on 28/10/2021.
//

import SwiftUI

struct CharacterDetailView: View {
    @StateObject private var episodesVM = EpisodesViewModel()
    
    var character: Character
    var episodes: [Episode]
    
    var body: some View {
            VStack {
                AsyncImage(url: URL(string: character.image)) { image in
                    CircleImage(image: image.resizable(),
                                radius: 7,
                                contentWidth: 200
                                , contentHeight: 200)
                } placeholder: {
                    ProgressView()
                }
                
                HStack(alignment: .center) {
                    getCharacterGenderImage()
                        .resizable()
                        .frame(width: 30, height: 30)
                        .padding()
                    
                    Text(character.species.rawValue)
                        .font(.title)
                        .padding()
                    
                    Text(getCharacterLifeStatus())
                        .font(.title)
                        .padding()
                }
                
                HStack {
                    VStack(alignment: .leading, spacing: 5) {
                        Text("Origin")
                            .font(.title2)
                            .fontWeight(.medium)
                        Text(character.origin.name)
                            .font(.title)
                            .fontWeight(.semibold)
                    }
                    Spacer()
                }.padding(.leading, 20)
                
                List(self.episodesVM.episodes) { episode in
                    EpisodeRow(episode: episode)
                }.onAppear {
                    self.episodesVM.getAllEpisodes(in: character.getEpisodesRange())
                }

            }.navigationTitle(Text(character.name))
            .navigationBarTitleDisplayMode(.inline)
            
    }
    
    private func getCharacterGenderImage() -> Image {
        return character.gender == .Male ? Image("male") : Image("female")
    }
    
    private func getCharacterLifeStatus() -> String {
        return character.isAlive() ? "Alive" : "Dead"
    }
    
    private func getIsCharacterFavImage() -> Image{
        return Image("favorite")
    }
    
}
