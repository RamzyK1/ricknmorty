//
//  CharactersListView.swift
//  Rick and Morty
//
//  Created by Ramzy Kermad on 28/10/2021.
//

import SwiftUI

struct CharactersListView: View {
    @StateObject private var charactersVM = CharactersViewModel()
    
    @State var showFiltersView = false
    @State var genderFilter: Gender? = nil
    @State var lifeStatusFilter: String? = nil
    
    var body: some View {
        NavigationView {
            VStack {
                CharacterList(characters: self.filterCharacters())
                    .navigationBarTitle(Text("Characters"))
                    .navigationBarItems(trailing:
                        Button(action: { showFiltersView.toggle() }) {
                            if(showFiltersView){
                                Text("Hide filters")
                            }else{
                                Text("Show filters")
                            }
                        }
                    )
                
                if(showFiltersView){
                    CharacterFilterView { gender in
                        self.genderFilter = gender
                    } statusFilter: { status in
                        self.lifeStatusFilter = status
                    }
                }
            }
        }.onAppear {
            self.charactersVM.getAllCHaracters()
        }
    }
    
    
    func filterCharacters() -> [Character]{
        return self.charactersVM.characters.filter { character in
            if(self.genderFilter != nil){
                return character.gender == genderFilter
            }
            return true
        }.filter { character in
            if(self.lifeStatusFilter != nil){
                return character.status == lifeStatusFilter
            }
            return true
        }
    }
}

struct CharactersListView_Previews: PreviewProvider {
    static var previews: some View {
        CharactersListView()
            .environmentObject(CharactersService())
    }
}
