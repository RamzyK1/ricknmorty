//
//  CharactersViewModel.swift
//  Rick and Morty
//
//  Created by Ramzy Kermad on 29/10/2021.
//

import Foundation
import SwiftUI

class CharactersViewModel: ObservableObject {
    private let characterService = CharactersService()
    @Published var characters: [Character] = []
    
    func getAllCHaracters(){
        characterService.getAllCharacters { characters in
            self.characters = characters
        }
    }
    
}
