//
//  EpisodesViewModel.swift
//  Rick and Morty
//
//  Created by Ramzy Kermad on 29/10/2021.
//

import Foundation
import SwiftUI

class EpisodesViewModel: ObservableObject {
    private let episodeService = EpisodesService()
    @Published var episodes: [Episode] = []
    
    func getAllEpisodes(in range: String){
        episodeService.getCharacterEpisodes(for: range, completion: { episodes in
            self.episodes = episodes
        })
    }
}
