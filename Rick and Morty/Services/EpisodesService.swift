//
//  EpisodesService.swift
//  Rick and Morty
//
//  Created by Ramzy Kermad on 29/10/2021.
//

import Foundation

class EpisodesService: ObservableObject {
    private let baseUrl = "https://rickandmortyapi.com/api/episode/"
    
    func getCharacterEpisodes(for range: String, completion: @escaping ([Episode]) -> Void) {
        let completUrl = "\(baseUrl)\(range)"
        guard let url = URL(string: completUrl) else { fatalError("Missing URL") }

        let urlRequest = URLRequest(url: url)

        let dataTask = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if let error = error {
                print("Request error: ", error)
                return
            }

            guard let response = response as? HTTPURLResponse else { return }

            if response.statusCode == 200 {
                guard let data = data else { return }
                DispatchQueue.main.async {
                    do {
                        let decodeEpisodes = try JSONDecoder().decode([Episode].self, from: data)
                        completion(decodeEpisodes)
                    } catch let error {
                        print("Error decoding: ", error)
                    }
                }
            }
        }

        dataTask.resume()
    }
}

