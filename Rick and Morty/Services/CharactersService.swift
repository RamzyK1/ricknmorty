//
//  CharactersService.swift
//  Rick and Morty
//
//  Created by Ramzy Kermad on 29/10/2021.
//

import Foundation
import SwiftUI

class CharactersService: ObservableObject {
    private let baseUrl = "https://rickandmortyapi.com/api/character"
    
    func getAllCharacters(completion: @escaping ([Character]) -> Void) {
        guard let url = URL(string: baseUrl) else { fatalError("Missing URL") }

        let urlRequest = URLRequest(url: url)

        let dataTask = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if let error = error {
                print("Request error: ", error)
                return
            }

            guard let response = response as? HTTPURLResponse else { return }

            if response.statusCode == 200 {
                guard let data = data else { return }
                DispatchQueue.main.async {
                    do {
                        let decodedCharacters = try JSONDecoder().decode(CharacterResponseModel.self, from: data)
                        completion(decodedCharacters.results)
                    } catch let error {
                        print("Error decoding: ", error)
                    }
                }
            }
        }

        dataTask.resume()
    }
}
