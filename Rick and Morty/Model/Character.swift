//
//  Character.swift
//  Rick and Morty
//
//  Created by Ramzy Kermad on 27/10/2021.
//

import Foundation
import SwiftUI

struct Character: Decodable, Identifiable {
    var id: Int
    var name: String
    var status: String
    var species: Species
    var gender: Gender
    var image: String
    var origin: CharacterOrigin
    var episode: [String]
    
    /**
     Function to now the range of episodes that the caracter appears in
     */
    func getEpisodesRange() -> String {
        var characterEpisodes = ""
        
        for element in episode {
            characterEpisodes += element.components(separatedBy: "episode/")[1]
            characterEpisodes += ","
        }
        // Remove last coma
        characterEpisodes.removeLast()
        return characterEpisodes
    }
    
    func isAlive() -> Bool {
        return status == "Alive"
    }
}

struct CharacterOrigin: Decodable {
    var name: String
    var url: String
}

enum Species: String, Decodable {
    case Human = "Human"
    case Alien = "Alien"
}

enum Gender: String, Decodable {
    case Male = "Male"
    case Female = "Female"
    case unknown = "unknown"
}
