//
//  CharacterResponseModel.swift
//  Rick and Morty
//
//  Created by Ramzy Kermad on 29/10/2021.
//

import Foundation

class CharacterResponseModel: Decodable{
    var info: ResponseInfo
    var results: [Character]
}

struct ResponseInfo: Decodable {
    var count: Int
    var pages: Int
    var next: String
}
