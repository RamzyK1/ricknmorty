//
//  Episode.swift
//  Rick and Morty
//
//  Created by Ramzy Kermad on 27/10/2021.
//

import Foundation
import SwiftUI

struct Episode: Hashable, Codable, Identifiable {
    var id: Int
    var name: String
    var episode: String
    var air_date: String
    
    
}
