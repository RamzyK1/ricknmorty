//
//  Rick_and_MortyApp.swift
//  Rick and Morty
//
//  Created by Ramzy Kermad on 27/10/2021.
//

import SwiftUI

@main
struct Rick_and_MortyApp: App {
    var characterService = CharactersService()
    var episodeService = EpisodesService()
    init(){
        print("ON EST PAS EN PROD")
    }
    
    var body: some Scene {
        WindowGroup {
            CharactersListView()
                .environmentObject(characterService)
                .environmentObject(episodeService)
        }
    }
}
