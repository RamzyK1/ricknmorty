//
//  CharacterList.swift
//  Rick and Morty
//
//  Created by Ramzy Kermad on 28/10/2021.
//

import SwiftUI

struct CharacterList: View {
    var characters: [Character] = []
    var episode: [Episode] = []
    
    var body: some View {
        List(characters, id: \.id) { character in
            NavigationLink(destination: CharacterDetailView(character: character,
                                                            episodes: episode)){
                CharacterRow(character: character)
            }
        }
    }
}

struct CharacterList_Previews: PreviewProvider {
    static var previews: some View {
        CharacterList()
    }
}
