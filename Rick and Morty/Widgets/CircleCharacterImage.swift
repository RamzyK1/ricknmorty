//
//  CircleCharacterImage.swift
//  Rick and Morty
//
//  Created by Ramzy Kermad on 28/10/2021.
//

import SwiftUI

struct CircleImage: View {
    var image: Image
    var radius: CGFloat
    var contentWidth: CGFloat
    var contentHeight: CGFloat

    var body: some View {
        image
            .frame(width: contentWidth, height: contentHeight, alignment: .center)
            .clipShape(Circle())
            .overlay(Circle().stroke(Color.white, lineWidth: 1))
            .shadow(radius: radius)
    }
}
