//
//  CharacterFilterView.swift
//  Rick and Morty
//
//  Created by Ramzy Kermad on 29/10/2021.
//

import SwiftUI

struct CharacterFilterView: View {
    private var updateGenderFilter: ((Gender?) -> ())
    private var updateLifeStatusFilter: ((String?) -> ())
    
    init(genderFilter: @escaping ((Gender?) -> ()),
         statusFilter: @escaping ((String?) -> ())) {
        
        self.updateGenderFilter = genderFilter
        self.updateLifeStatusFilter = statusFilter
    }
    
    var body: some View {
        HStack {
            Text("Gender")
                .frame(maxWidth: .infinity)
            Button(action: {
                self.updateGenderFilter(.Male)
            }) {
                Image("male")
                    .resizable()
                    .frame(width: 20, height: 20)
                    .frame(maxWidth: .infinity)
            }
            Button(action: {
                self.updateGenderFilter(.Female)
            }) {
                Image("female")
                    .resizable()
                    .frame(width: 20, height: 20)
                    .frame(maxWidth: .infinity)
            }
            Button(action: {
                self.updateGenderFilter(nil)
            }) {
                Text("All")
                    .frame(maxWidth: .infinity)
            }
        }
        
        HStack {
            Text("Life status")
                .frame(maxWidth: .infinity)
            Button(action: {
                self.updateLifeStatusFilter("Alive")
            }) {
                Text("Alive")
                    .frame(maxWidth: .infinity)
            }
            Button(action: {
                self.updateLifeStatusFilter("Dead")
            }) {
                Text("Dead")
                    .frame(maxWidth: .infinity)
            }
            Button(action: {
                self.updateLifeStatusFilter(nil)
            }) {
                Text("All")
                    .frame(maxWidth: .infinity)
            }
        }
    }
}
