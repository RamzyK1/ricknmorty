//
//  CharacterRow.swift
//  Rick and Morty
//
//  Created by Ramzy Kermad on 27/10/2021.
//

import SwiftUI

struct CharacterRow: View {
    var character: Character
    
    var body: some View {
        HStack {
            AsyncImage(url: URL(string: character.image)
            ) { image in
                CircleImage(
                    image: image.resizable(),
                    radius: 7, contentWidth: 50, contentHeight: 50)
            } placeholder: {
                ProgressView()
            }.frame(width: 50, height: 50)
            
            
            
            VStack(alignment: .leading, spacing: 6) {
                Text("\(character.name) - \(character.isAlive() ? "Alive" : "Dead")")
                Text(character.species.rawValue)
            }
            Spacer()
            
            character.gender.rawValue == "Male" ?
            Image("male")
                .resizable()
                .frame(width: 20, height: 20)
                .padding(.trailing, 10)
            : Image("female")
                .resizable()
                .frame(width: 20, height: 20)
                .padding(.trailing, 10)
        }
    }
    
}
