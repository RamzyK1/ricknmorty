//
//  EpisodeRow.swift
//  Rick and Morty
//
//  Created by Ramzy Kermad on 28/10/2021.
//

import SwiftUI

struct EpisodeRow: View {
    var episode: Episode
    
    var body: some View {
        HStack {
            VStack(alignment: .leading, spacing: 5) {
                Text(episode.name)
                    .font(.title2)
                    .fontWeight(.medium)
                Text(episode.air_date)
                    .font(.footnote)
                    .fontWeight(.thin)
            }
            Spacer()
            Text(episode.episode)
        }
    }
}

struct EpisodeRow_Previews: PreviewProvider {
    static var previews: some View {
        EpisodeRow(episode: Episode(id: 1,
                                    name: "Name",
                                    episode: "S01E01",
                                    air_date: "December2, 2013"))
    }
}
