//
//  Rick_and_MortyTests.swift
//  Rick and MortyTests
//
//  Created by Ramzy Kermad on 27/10/2021.
//

import XCTest
@testable import rickNmorty_DEBUG

class Rick_and_MortyTests: XCTestCase {
    var characterOrigin: CharacterOrigin!
    var character: Character!
    var episode: Episode!
    
    override func setUpWithError() throws {
        characterOrigin = CharacterOrigin(name: "Earth", url: "")
        character = Character(id: 1,
                              name: "Character 1",
                              status: "Alive",
                              species: .Human,
                              gender: .Male,
                              image: "",
                              origin: characterOrigin,
                              episode: [
                                   "https://rickandmortyapi.com/api/episode/1",
                                   "https://rickandmortyapi.com/api/episode/2",
                                   "https://rickandmortyapi.com/api/episode/3",
                                   "https://rickandmortyapi.com/api/episode/4",
                                   "https://rickandmortyapi.com/api/episode/5",
                                   "https://rickandmortyapi.com/api/episode/6",
                                   "https://rickandmortyapi.com/api/episode/7",
                                   "https://rickandmortyapi.com/api/episode/8"
                              ])
        episode = Episode(id: 1, name: "Episode 1", episode: "S01E01", air_date: "23/10/2021")
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testCharacterIsCreated(){
        XCTAssertNotNil(character.name, "Test character name exists")
        XCTAssertNotNil(character.status, "Test character life status exists")
        XCTAssertNotNil(character.species, "Test character species exists")
        XCTAssertNotNil(character.gender, "Test character gender exists")
        XCTAssertNotNil(character.origin, "Test character origin exists")
        XCTAssertNotNil(character.episode, "Test character episode exists")
    }
    
    func testCharacterEpisodesRangeIsCorrect() throws{
        let expectedEpisodeRange = "1,2,3,4,5,6,7,8"
        
        XCTAssertEqual(expectedEpisodeRange, character.getEpisodesRange(), "Testing character apparition in the range of episodes")
    }
    
    func testCharacterIsAlive(){
        XCTAssertEqual(character.isAlive(), true, "Testing if character is Alive")
    }
    
    func testCharacterisDead(){
        let character2 = Character(id: 2,
                              name: "Character 2",
                              status: "Dead",
                              species: .Human,
                              gender: .Male,
                              image: "",
                              origin: characterOrigin,
                              episode: [
                                   "https://rickandmortyapi.com/api/episode/1",
                                   "https://rickandmortyapi.com/api/episode/2",
                              ])
        XCTAssertEqual(character2.isAlive(), false, "Testing if character is Dead")
    }
    
    func testEpisodeCreated(){
        XCTAssertNotNil(episode.id, "The episode should have an id")
        XCTAssertNotNil(episode.name, "The episode should have an name")
        XCTAssertNotNil(episode.episode, "The episode should have an episode number")
        XCTAssertNotNil(episode.air_date, "The episode should have date")
    }
    
    func createCharacters() -> [Character] {
        var characters: [Character] = []
        
        for i in 0...5{
            characters.append(Character(id: i,
                                        name: "Character \(i)",
                                        status: i%2 == 0 ? "Alive" : "Dead",
                                        species: i%3 == 0 ? .Alien : .Human,
                                        gender: i%3 == 0 ? .Female : .Male,
                                        image: "",
                                        origin: characterOrigin,
                                        episode: [
                                            "https://rickandmortyapi.com/api/episode/1",
                                            "https://rickandmortyapi.com/api/episode/2",
                                            "https://rickandmortyapi.com/api/episode/3"
                                        ]))
        }
        
        return characters
    }

}
